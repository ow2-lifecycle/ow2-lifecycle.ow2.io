---
layout: default
---

# {{ site.title }}
----

## Home page

This is the site index page.

This is only a demo site with some OW2 look and feel.  
Note that this is not a Jekyll OW2 theme but rather a site with a customized layout and style. Potential improvement would be to package the layout and style as a Jekyll theme.